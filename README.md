# html-css-task
Assigned Task: [Scrimba's Intro HTML/CSS](https://gitlab.com/grey-software/students/-/issues/9)
- [x] Lesson 27 Creating Columns with flexbox
- [x] Lesson 28 Creating HTML Layout
- [x] Lesson 29 Adding CSS to the Layout
